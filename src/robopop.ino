#include <Arduino.h>
#include "Strip.h"

#define LEFT_STRIP_N_LEDS    5
#define RIGHT_STRIP_N_LEDS    5
#define CENTRAL_STRIP_N_LEDS    9
#define LEFT_STRIP_PIN 3
#define CENTRAL_STRIP_PIN 5
#define RIGHT_STRIP_PIN 4
#define BTN_PIN   6
#define LED_PIN   13 // blink 'digital' pin 1 - AKA the built in red LED

RoboStrip central_strip = RoboStrip(CENTRAL_STRIP_N_LEDS, CENTRAL_STRIP_PIN);
RoboStrip left_strip = RoboStrip(LEFT_STRIP_N_LEDS, LEFT_STRIP_PIN);
RoboStrip right_strip = RoboStrip(RIGHT_STRIP_N_LEDS, RIGHT_STRIP_PIN);

void setup()
{
  Serial.begin(9600);
  // Timer0 is already used for millis() - we'll just interrupt somewhere
  // in the middle and call the "Compare A" function below
  OCR0A = 0xAF;
  TIMSK0 |= _BV(OCIE0A);
  pciSetup(BTN_PIN);

}

// Install Pin change interrupt for a pin, can be called multiple times

void pciSetup(byte pin)
{
  *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));  // enable pin
  PCIFR  |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
  PCICR  |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
}

// Interrupt is called once a millisecond,
SIGNAL(TIMER0_COMPA_vect)
{
  unsigned long update_time = millis();
  left_strip.Update(update_time);
  central_strip.Update(update_time);
  right_strip.Update(update_time);
}

ISR (PCINT2_vect) // handle pin change interrupt for D0 to D7
{
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 200)
  {
    left_strip.NextMode();
    central_strip.NextMode();
    right_strip.NextMode();
  }
  last_interrupt_time = interrupt_time;
}


// We don't do anything in the loop, we do everything on interrupts
void loop() {}
