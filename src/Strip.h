#include "Adafruit_NeoPixel.h"
class RoboStrip  : public Adafruit_NeoPixel
{

  public:
    RoboStrip(uint16_t n, uint8_t p=6, uint8_t t=NEO_GRB + NEO_KHZ800);
    void NextMode();
    void Update(unsigned long update_time);
  private:
    void Update_Warning(unsigned long update_time);
    void Update_Searchlight(unsigned long update_time);
    void Update_Flash(unsigned long update_time);
    void Update_Niancat(unsigned long update_time);
    void Update_Scan(unsigned long update_time);
    uint32_t Wheel(byte WheelPos);

};