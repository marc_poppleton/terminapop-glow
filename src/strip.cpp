// strip.cpp
#include "Strip.h"

  #define MODE_FLASH    1
  #define MODE_SCANNER  2
  #define MODE_NIANCAT  3
  #define MODE_WHITE    4
  #define MODE_WARNING  5

  unsigned long next_flash_delay;
  unsigned long flash_speed;
  int mode = MODE_FLASH; // default mode is flash
  int pos = 0,dir = 1;

  unsigned long last_update_time = 0;

  // Input a value 0 to 255 to get a color value.
  // The colours are a transition r - g - b - back to r.
  uint32_t RoboStrip::Wheel(byte WheelPos)
  {
    WheelPos = 255 - WheelPos;
    if (WheelPos < 85)
    {
      return this->Color(255 - WheelPos * 3, 0, WheelPos * 3);
    }
    else if (WheelPos < 170)
    {
      WheelPos -= 85;
      return this->Color(0, WheelPos * 3, 255 - WheelPos * 3);
    }
    else
    {
      WheelPos -= 170;
      return this->Color(WheelPos * 3, 255 - WheelPos * 3, 0);
    }
  }

  void RoboStrip::Update_Warning(unsigned long update_time)
  {
    if(update_time - last_update_time > 63){
      for (int x = 0; x < 3; x++) {
        for (uint16_t i = 0; i < this->numPixels(); i++)
        {
          this->setPixelColor(i, 0xdb650a);
        }
        this->show();
      }
    }else{
      for (uint16_t i = 0; i < this->numPixels(); i++)
      {
        this->setPixelColor(i, 0x0);
      }
      this->show();
    }
  //TODO Add 500ms pause between blink cycle
  }

  void RoboStrip::Update_Searchlight(unsigned long update_time)
  {
    for (uint16_t i = 0; i < this->numPixels(); i++)
    {
      this->setPixelColor(i, 0xFFFFFF);
    }
    this->show();
  }

  void RoboStrip::Update_Flash(unsigned long update_time)
  {
    int j;
    if(pos>-6)
    {

      // Draw 5 pixels centered on pos.  setPixelColor() will clip any
      // pixels off the ends of the strip, we don't need to watch for that.
      this->setPixelColor(pos + 4, 0x100000); // Dark red
      this->setPixelColor(pos + 3, 0x800000); // Medium red
      this->setPixelColor(pos + 2, 0xcc0000); // Medium red
      this->setPixelColor(pos + 1, 0xee0000); // Medium red
      this->setPixelColor(pos    , 0xFF0000); // Center pixel is brightest
      this->setPixelColor(pos - 1, 0x800000); // Medium red

      this->show();

      //TODO add flash speed delay
      //do {} while (millis() - timer < flash_speed);

      // Rather than being sneaky and erasing just the tail pixel,
      // it's easier to erase it all and draw a new one next time.
    for (j = -1; j <= 4; j++) this->setPixelColor(pos + j, 0);

      // Bounce off ends of strip
      pos--;
    }
    else{
      pos = this->numPixels();
      flash_speed = random(2, 20);
      next_flash_delay = random(500, 1500);
    }

    //TODO Add in between flashes delay
    //while (millis() - timer < flash_delay);
  }

  void RoboStrip::Update_Niancat(unsigned long update_time)
  {
    pos = 0;
    if(pos<255) {
      for (int i = this->numPixels(); i > 0 ; i--)
      {
        this->setPixelColor(i, Wheel(((i * 256 / this->numPixels() + pos)) & 255));
      }
      this->show();
      pos++;
    }
    else
    {
      pos = 0;
    }
  }

  void RoboStrip::Update_Scan(unsigned long update_time)
  {
    if(update_time - last_update_time > 30){

    // Draw 5 pixels centered on pos.  setPixelColor() will clip any
    // pixels off the ends of the strip, we don't need to watch for that.
    this->setPixelColor(pos - 2, 0x100000); // Dark red
    this->setPixelColor(pos - 1, 0x800000); // Medium red
    this->setPixelColor(pos    , 0xFF3000); // Center pixel is brightest
    this->setPixelColor(pos + 1, 0x800000); // Medium red
    this->setPixelColor(pos + 2, 0x100000); // Dark red

    this->show();

    // Bounce off ends of strip
    pos += dir;
    if (pos < 0) {
      pos = 1;
      dir = -dir;
    } else if (pos >= this->numPixels()) {
      pos = this->numPixels() - 2;
      dir = -dir;
    }
  }
  else{
    // Rather than being sneaky and erasing just the tail pixel,
    // it's easier to erase it all and draw a new one next time.
    for (int j = -2; j <= 2; j++) this->setPixelColor(pos + j, 0);
  }
}

RoboStrip::RoboStrip(uint16_t n, uint8_t p, uint8_t t) : Adafruit_NeoPixel(n, p, t)
{
}

void RoboStrip::NextMode() {
  if (mode == MODE_WARNING) {
    mode = MODE_FLASH;
  } else {
    mode++;
  }
}

void RoboStrip::Update(unsigned long update_time) {
  switch (mode) {
    case MODE_NIANCAT:
      Update_Niancat(update_time);
      break;
    case MODE_WHITE:
      Update_Searchlight(update_time);
      break;
    case MODE_WARNING:
      Update_Warning(update_time);
      break;
    case MODE_SCANNER:
      Update_Scan(update_time);
      break;
    case MODE_FLASH:
    default:
      Update_Flash(update_time);
      break;
  }
  last_update_time = update_time;
}